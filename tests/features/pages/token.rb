class Token 

  include Appium::Core

  def initialize
    @token = ""
    @tokentext = ""
    @arquivo = "token.txt"
  end

  # def acessar_mobile
  #   Wait.until { find_element(id: "br.com.omni.omni:id/etCpf").send_keys("71707649030") }
  #   Wait.until { find_element(id: "br.com.omni.omni:id/btnNext").click }
  #   Wait.until { find_element(id: "br.com.omni.omni:id/etPassword") }
  #   Wait.until { find_element(id: "br.com.omni.omni:id/btnToken") }
  # end

  # def clicar_token
  #   Wait.until { find_element(id: "br.com.omni.omni:id/btnToken").click }
  # end

  # def pegar_token
  #     # begin
  #       # starting = Time.now

  #       tokentext = find_element(id: "br.com.omni.omni:id/tvTokenNumber").text
  #       tokentextgsub = tokentext.gsub(" ", "")

  #       # ending = Time.now
  #       # elapsed = ending - starting
  #     # end

  #     # begin
  #     #   starting = Time.now
  #     #   tokentext = find_element(id: "br.com.omni.omni:id/tvTokenNumber")
  #     #   ending = Time.now
  #     #   elapsed = ending - starting
  #     # end

  #   return tokentextgsub
  # end

  def acessar_mobile
    Wait.until { find_element(id: "br.com.omni.omni:id/etCpf").send_keys("71707649030") }
    Wait.until { find_element(id: "br.com.omni.omni:id/btnNext").click }
    Wait.until { find_element(id: "br.com.omni.omni:id/etPassword") }
    Wait.until { find_element(id: "br.com.omni.omni:id/btnToken") }
  end
  
  def clicar_token
    Wait.until { find_element(id: "br.com.omni.omni:id/btnToken").click }
  end

  def pegar_token
    tokentext = ""
    Wait.until { tokentext = find_element(id: "br.com.omni.omni:id/tvTokenNumber").text.gsub(" ", "") }
    return tokentext
  end

  def gravar_token_txt(token)
    if(File.exist?(@arquivo))
      File.write(@arquivo, "#{token}\n#{Time.now}")
    else
      @arquivo = File.new(@arquivo, "w")
      @arquivo.puts("#{token}\n#{Time.now}")
      @arquivo.close
    end
  end

  def pegar_token_antigo
    arquivoaberto = File.open(@arquivo)
    tokenantigo = arquivoaberto.read

    tokenantigo.nil? ? tokenantigo = "000000" : tokenantigo

    return tokenantigo.split("\n")[0]
  end

end