Quando('eu acesso a aplicacao mobile') do
  @token = Token.new
  @token.acessar_mobile
  @token.clicar_token
end

Quando('capturo o token e alimento o arquivo txt') do
  while true
    begin
      tokennovo = @token.pegar_token
    rescue Exception => ex
      log(ex.message)
    end

    tokenantigo = @token.pegar_token_antigo

    if tokennovo != tokenantigo
      @token.gravar_token_txt(tokennovo)
    end
  end
end