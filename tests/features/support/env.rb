require 'capybara/cucumber'
require_relative 'helper.rb'
require 'capybara'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require 'appium_capybara'
require 'appium_lib'
require 'pry'

def caps
{   caps: {
        deviceName: "emulator-5554",
        platformName: "Android",
        noReset: "true",
        fullReset: "false",
        appActivity: "br.com.omni.omni.view.access.SplashActivity",
        automationName: "UIAutomator2",
        appPackage: "br.com.omni.omni",
        newCommandTimeout: 3600,
    }
}
end

Appium::Driver.new(caps, true)
Appium.promote_appium_methods Object